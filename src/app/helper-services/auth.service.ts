import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

const BASE_URL = environment.baseUrl;

@Injectable({
    providedIn: 'root'
})

export class AuthService {

    constructor(
        private router: Router,
        private http: HttpClient
    ) { }

    isUserLoggedIn(): boolean {
        let token = localStorage.getItem('access_token');
        if (token) {
            return true
        } else {
            return false
        }
    }

    logout() {
        localStorage.removeItem('access_token');
        this.router.navigateByUrl('login');
    }

    whoAmI(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${BASE_URL}/getMyProfile`)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
