import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { AppAuthGuard } from 'app/app.authguard';
import { DashboardComponent } from './dashboard.component';

const routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AppAuthGuard]
    }
];

@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ]
})

export class DashboardModule {
}
