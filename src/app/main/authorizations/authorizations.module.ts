import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { FuseConfirmDialogModule, FuseWidgetModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { AuthorizationService } from './authorization.service';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LoginComponent } from './login/login.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { AppAuthGuard } from 'app/app.authguard';

const routes: Routes = [
    {
        path: 'auth',
        canActivate: [AppAuthGuard],
        children: [
            {
                path: 'login',
                component: LoginComponent
            },
            {
                path: 'forgot-password',
                pathMatch: 'full',
                component: ForgetPasswordComponent
            },
            {
                path: '**',
                component: LoginComponent,
                canActivate: [AppAuthGuard]
            }
        ]
    },
    {
        path: 'reset-password/:id',
        component: ResetPasswordComponent
    }
];

@NgModule({
    declarations: [
        LoginComponent,
        ForgetPasswordComponent,
        ResetPasswordComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forRoot(routes),
        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSnackBarModule,
        MatMenuModule,
        MatDialogModule,
        FuseConfirmDialogModule,
        FuseSharedModule,
        FuseWidgetModule,
        FormsModule,
        ReactiveFormsModule,
        MatCheckboxModule
    ],
    providers: [
        AuthorizationService
    ]
})

export class AuthorizationsModule {
}
