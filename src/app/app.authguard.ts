import { AuthService } from './helper-services/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class AppAuthGuard implements CanActivate {

  constructor(
    private _router: Router,
    private authService: AuthService,
    private matSnackBar: MatSnackBar
  ) {
  }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    // TOKEN EXISTS
    if (state.url.includes('auth') && this.authService.isUserLoggedIn()) {
      this._router.navigateByUrl('/dashboard');
      return true
    } 
    // TOKEN NOT EXISTS
    else if (!state.url.includes('auth') && !this.authService.isUserLoggedIn()) {
      this._router.navigateByUrl('/auth/login');
      return true
    }
    return true;
  }

  accessNotAllowed() {
    this.matSnackBar.open("Access not allowed", "Ok", {
      duration: 2000,
      verticalPosition: 'bottom'
    })
  }

}
