const firebaseConfig = {
    // FIREBASE OBJECT KEYS HERE
};

export const environment = {
    baseUrl: 'https://abc.com',
    hmr: false,
    secure: false,
    production: true,
    firebase: firebaseConfig,
    version: 'jenkinsBuildNO'
};