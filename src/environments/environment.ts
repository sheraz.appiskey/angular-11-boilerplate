const firebaseConfig = {
    // FIREBASE OBJECT KEYS HERE
};

export const environment = {
    baseUrl: 'https://abc.com',
    hmr: false,
    secure: false,
    production: false,
    firebase: firebaseConfig,
    version: 'jenkinsBuildNO'
};